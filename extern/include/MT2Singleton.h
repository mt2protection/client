#ifndef __MT2PROTECTION_mt2_singleton_H__
#define __MT2PROTECTION_mt2_singleton_H__

#include <assert.h>

template <typename T> class CMT2Singleton 
{ 
	static T * mt2s_singleton;
	
public: 
	CMT2Singleton()
	{ 
		assert(!mt2s_singleton);
		int offset = (int) (T*) 1 - (int) (CMT2Singleton <T>*) (T*) 1; 
		mt2s_singleton = (T*) ((int) this + offset);
	} 

	virtual ~CMT2Singleton()
	{ 
		assert(mt2s_singleton);
		mt2s_singleton = 0; 
	}

	__forceinline static T & Instance()
	{
		assert(mt2s_singleton);
		return (*mt2s_singleton);
	}

	__forceinline static T * InstancePtr()
	{ 
		return (mt2s_singleton);
	}

	__forceinline static T & instance()
	{
		assert(mt2s_singleton);
		return (*mt2s_singleton);
	}
};

template <typename T> T * CMT2Singleton <T>::mt2s_singleton = 0;

//
// mt2_singleton for non-hungarian
//
template <typename T> class mt2_singleton
{ 
	static T * mt2s_singleton;
	
public: 
	mt2_singleton()
	{ 
		assert(!mt2s_singleton);
		int offset = (int) (T*) 1 - (int) (mt2_singleton <T>*) (T*) 1; 
		mt2s_singleton = (T*) ((int) this + offset);
	} 

	virtual ~mt2_singleton()
	{ 
		assert(mt2s_singleton);
		mt2s_singleton = 0; 
	}

	__forceinline static T & Instance()
	{
		assert(mt2s_singleton);
		return (*mt2s_singleton);
	}

	__forceinline static T * InstancePtr()
	{ 
		return (mt2s_singleton);
	}

	__forceinline static T & instance()
	{
		assert(mt2s_singleton);
		return (*mt2s_singleton);
	}
};

template <typename T> T * mt2_singleton <T>::mt2s_singleton = 0;

#endif
