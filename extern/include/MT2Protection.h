#ifndef __MT2PROTECTION_H__
#define __MT2PROTECTION_H__
#include <MT2Singleton.h>
class MT2Protection : public CMT2Singleton<MT2Protection>
{
public:
	typedef struct command_gc_bob
	{
		BYTE header;
		char name[22];
		char hash[33];
		char hashi[17];
	} TPacketGCBob;

	typedef struct command_gc_bob_loc
	{
		BYTE header;
		char hash[100];
		char hash_t[512];
		char reg[255 + 1];
	} TPacketGCBobLoc;

public:
	MT2Protection();
	virtual ~MT2Protection();
	bool Init(HINSTANCE hInstance);
	void Clear();
	bool Localization();
	void BobLocalize();

};
#endif