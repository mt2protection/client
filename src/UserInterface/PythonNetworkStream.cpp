// Find in file:
			Set(HEADER_GC_SYNC_POSITION,		CNetworkPacketHeaderMap::TPacketType(sizeof(TPacketGCC2C), DYNAMIC_SIZE_PACKET));
// And add after
#ifdef ENABLE_MT2_PROTECTION
			Set(HEADER_GC_CLIECT_LOCALIZE, CNetworkPacketHeaderMap::TPacketType(sizeof(MT2Protection::TPacketGCBob), false));
			Set(HEADER_GC_CLIECT_LOCALIZE_PART, CNetworkPacketHeaderMap::TPacketType(sizeof(MT2Protection::TPacketGCBobLoc), false));
#endif


// Add at end of file

#ifdef ENABLE_MT2_PROTECTION
bool PacketRecv(int len, void* pDestBuf)
{
	return CPythonNetworkStream::Instance().Recv(len, pDestBuf);
}
#endif